<!-- resources/views/time_entries/index.blade.php -->
@extends('layouts.app')

@section('content')
    <h2>Time Entry</h2>
    <table>
        <thead>
            <tr>
                <th>SNo</th>
                <th>Project Name</th>
                <th>Task Name</th>
                <th>Hours</th>
                <th>Date</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach($timeEntries as $timeEntry)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $timeEntry->task->project->name }}</td>
                    <td>{{ $timeEntry->task->name }}</td>
                    <td>{{ $timeEntry->hours }}</td>
                    <td>{{ $timeEntry->date }}</td>
                    <td>{{ $timeEntry->description }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
