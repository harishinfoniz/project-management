<!-- resources/views/time_entries/create.blade.php -->

@extends('layouts.app')

@section('content')
    <h2>Add New Time Entry</h2>
    
    <form action="{{ route('time-entries.store') }}" method="post">
        @csrf

        <div class="form-group">
            <label for="task_id">Task:</label>
            <select name="task_id" id="task_id" class="form-control" required>
                @foreach ($tasks as $task)
                    <option value="{{ $task->id }}">{{ $task->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="hours">Hours:</label>
            <input type="number" name="hours" id="hours" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="date">Date:</label>
            <input type="date" name="date" id="date" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <textarea name="description" id="description" class="form-control" rows="3" required></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Add Time Entry</button>
    </form>
@endsection
