

@extends('layouts.app')

@section('content')
    <h2>Project Time Report</h2>

    <form id="filterForm">
        @csrf
        <label for="projectFilter">Filter by Project:</label>
        <select name="project_id" id="projectFilter">
            <option value="">All Projects</option>
            @foreach ($projects as $project)
                <option value="{{ $project->id }}">{{ $project->name }}</option>
            @endforeach
        </select>
        <button type="button" onclick="filterReport()">Apply Filter</button>
    </form>


    <div id="reportContent">
        @include('reports.report-content', ['projects' => $projects])
    </div>


    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script>
        function filterReport() {
            var projectId = document.getElementById('projectFilter').value;

            $.ajax({
                type: 'POST',
                url: '{{ route("reports.filter") }}',
                data: { project_id: projectId, _token: '{{ csrf_token() }}' },
                success: function (data) {
                    $('#reportContent').html(data.content);
                },
                error: function (error) {
                    console.error('Error: ', error);
                }
            });
        }
    </script>
@endsection
