
@foreach ($projects as $project)
    <h3>{{ $project->name }}</h3>
    <table>
        <thead>
            <tr>
                <th>Task</th>
                <th>Total Hours</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($project->tasks as $task)
                <tr>
                    <td>{{ $task->name }}</td>
                    <td>{{ $task->timeEntries->sum('hours') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endforeach
