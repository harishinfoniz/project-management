<!-- resources/views/layouts/app.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project Management System</title>
    <!-- Include your CSS stylesheets or CDN links here -->
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }

        header {
            background-color: #333;
            color: white;
            text-align: center;
            padding: 1em;
        }

        nav {
            background-color: #555;
            color: white;
            padding: 1em;
        }

        nav a {
            color: white;
            text-decoration: none;
            margin: 0 10px;
        }

        section {
            margin: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
    </style>
</head>
<body>
    <header>
        <h1>Project Management System</h1>
    </header>

    <nav>
        <a href="{{ route('projects.index') }}">Manage Projects</a>
        <a href="{{ route('tasks.index') }}">Manage Tasks</a>
        <a href="{{ route('time-entries.index') }}">Time Entry</a>
        <!-- Add more navigation links as needed -->
    </nav>

    <section>
        @yield('content')
    </section>
</body>
</html>
