<!-- resources/views/tasks/index.blade.php -->
@extends('layouts.app')

@section('content')
    <h2>Manage Tasks</h2>
    <table>
        <thead>
            <tr>
                <th>SNo</th>
                <th>Project Name</th>
                <th>Task Name</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tasks as $task)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $task->project->name }}</td>
                    <td>{{ $task->name }}</td>
                    <td>{{ $task->status }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
