<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::create(['name' => 'Project 1', 'status' => 'Active']);
        Project::create(['name' => 'Project 2', 'status' => 'Inactive']);
        Project::create(['name' => 'Project 3', 'status' => 'Active']);
        Project::create(['name' => 'project 4', 'status' => 'Active']);
        Project::create(['name' => 'Project 5', 'status' => 'Active']);

    }
}
