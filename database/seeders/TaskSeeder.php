<?php

namespace Database\Seeders;

use App\Models\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Task::create(['project_id' => 1, 'name' => 'Task 1', 'status' => 'Active']);
        Task::create(['project_id' => 1, 'name' => 'Task 2', 'status' => 'Active']);
        Task::create(['project_id' => 1, 'name' => 'Task 3', 'status' => 'Active']);
        Task::create(['project_id' => 4, 'name' => 'Task 4', 'status' => 'Active']);
        Task::create(['project_id' => 4, 'name' => 'Task 5', 'status' => 'Active']);
    }
}
