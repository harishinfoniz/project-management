<?php

namespace Database\Seeders;

use App\Models\TimeEntry;
use Illuminate\Database\Seeder;

class TimeEntrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TimeEntry::create(['task_id' => 1, 'hours' => 2, 'date' => '2021-02-02', 'description' => 'DB creation']);
        TimeEntry::create(['task_id' => 1, 'hours' => 6, 'date' => '2021-02-25', 'description' => 'Bug fixing']);
        TimeEntry::create(['task_id' => 2, 'hours' => 3, 'date' => '2021-03-28', 'description' => 'Testing']);
        TimeEntry::create(['task_id' => 4, 'hours' => 6, 'date' => '2020-03-03', 'description' => 'User Manager']);
        TimeEntry::create(['task_id' => 4, 'hours' => 4, 'date' => '2021-04-02', 'description' => 'Billing calculation']);
        TimeEntry::create(['task_id' => 5, 'hours' => 8, 'date' => '2020-05-07', 'description' => 'Login and Logout']);
    }
}
