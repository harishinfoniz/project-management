<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TimeEntryController;
use App\Http\Controllers\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('projects', ProjectController::class);
Route::resource('tasks', TaskController::class);
Route::resource('time-entries', TimeEntryController::class);
Route::get('reports', [ReportController::class, 'index'])->name('reports.index');
Route::post('reports/filter', [ReportController::class, 'filter'])->name('reports.filter');

