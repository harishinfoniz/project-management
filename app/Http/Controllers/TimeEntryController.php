<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\TimeEntry;
use Illuminate\Http\Request;

class TimeEntryController extends Controller
{
     public function index()
    {
        
        $timeEntries = TimeEntry::with('task.project')->get();
        return view('time_entries.index', compact('timeEntries'));
    }
    public function create()
    {
        $tasks = Task::all();
        return view('time_entries.create', compact('tasks'));
    }

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'task_id' => 'required',
            'hours' => 'required|numeric',
            'date' => 'required|date',
            'description' => 'required',
        ]);
        //Time entry storeing
        TimeEntry::create($validatedData);

        return redirect()->route('time-entries.index')->with('success', 'Time entry added successfully');
    }
}
