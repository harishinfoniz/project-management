<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class ReportController extends Controller
{
   
    public function index()
    {
        $projects = Project::with('tasks.timeEntries')->get();
        return view('reports.index', compact('projects'));
    }

    public function filter(Request $request)
    {
        $projectId = $request->input('project_id');

        $projects = Project::when($projectId, function ($query) use ($projectId) {
            $query->where('id', $projectId);
        })->with('tasks.timeEntries')->get();

        $view = view::make('reports.report-content', ['projects' => $projects]);
        $content = $view->render();

        return response()->json(['content' => $content]);
    }
}
